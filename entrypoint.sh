#!/bin/bash

if [ ! -f /opt/records.txt ]; then
  cat > /opt/records.txt <<-EOF
www.google.com A
www.gmail.com A
chat.google.com A
mail.google.com A
drive.google.com A
docs.google.com A
meet.google.com A
sites.google.com A
plus.google.com A
EOF
fi

dnsperf -d /opt/records.txt \
        $@
