# dnsperf

# How to run it?

## Docker

```bash
docker run -ti x11tete11x/dnsperf -s <DNS_SERVER_IP> -p <DNS_PORT> -l <BENCHMARK_TIME>
```

## K8S

```bash
kubectl run -i --tty dnsperf --restart=Never --rm --image=x11tete11x/dnsperf -- -s 1.1.1.1 -l 120
```
